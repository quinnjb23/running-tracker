package com.example.runningtracker

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.os.SystemClock
import android.view.View
import android.widget.Chronometer
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import kotlin.math.floor

open class RunActivity : AppCompatActivity() {
    private lateinit var chronometer: Chronometer
    private lateinit var distance: TextView
    private lateinit var pace: TextView


    private var pauseOffset: Long = 0
    private var running = false
    private var first = true
    private var dist = 0.00

    private var count = 0

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = 1000
    private lateinit var mLastLocation: Location
    private lateinit var newLocation: Location
    private lateinit var mLocationRequest: LocationRequest



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_run)

        mLocationRequest = LocationRequest()

        chronometer = findViewById(R.id.chronometer)
        pace = findViewById(R.id.pace)
        distance = findViewById(R.id.distance)
    }

    //Called when the 'Start' button is pressed
    //Starts the chronometer and location updates
    fun startRun(v: View) {
        if (!running) {
            if (first) {
                chronometer.base = SystemClock.elapsedRealtime()
                pauseOffset = 0
                dist = 0.00
                first = false
            }
            chronometer.base = SystemClock.elapsedRealtime() - pauseOffset
            chronometer.start()
            running = true

            startLocationUpdates()
        }
    }

    //Called when the 'Pause' button is pressed
    //Stops the chronometer and keeps track of the pauseOffset for when the chronometer is started again
    //Stops location updates
    fun pauseRun(v: View) {
        if (running) {
            chronometer.stop()
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.base
            running = false

            stopLocationUpdates()
        }
    }

    //Called when the 'Finish' button is pressed and after the run has been paused
    //Sends the distance and time to the 'PostRun' activity
    fun finishRun(v: View) {
        if (!running) {
            val intent = Intent(this, PostRun::class.java)

            val newDist = String.format("%.2f", dist)
            intent.putExtra("Distance", newDist)
            intent.putExtra("Time", msToTimeFormat(pauseOffset))

            startActivity(intent)
        }

    }


    //Starts location updates
    private fun startLocationUpdates() {
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback,
            Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationResult.lastLocation
            onLocationChanged(locationResult.lastLocation)
        }
    }

    //When the location is changed it grabs a new location and calculates a new distance and pace
    fun onLocationChanged(location: Location) {
        mLastLocation = location
        speedToPace(mLastLocation.speed)
        when (count) {
            0 -> {
                newLocation = location
                count++
            }
            1 -> {
                dist += newLocation.distanceTo(mLastLocation) / 1609.34
                newLocation = location
            }
        }
        val newDist = String.format("%.2f", dist)
        distance.text = "Distance: $newDist"

    }

    private fun stopLocationUpdates() {
        mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
    }

    //Convert speed given as meters per second to minutes per mile
    private fun speedToPace(f: Float) {
        var result: Double = f.toDouble()
        var minutes = 0
        var seconds = 0

        result = 26.8224 / result
        minutes = floor(result).toInt()
        seconds = floor((result - minutes) * 60).toInt()

        when {
            minutes > 30 -> pace.text = "Pace: -:--"
            seconds < 10 -> pace.text = "Pace: $minutes:0$seconds"
            else -> pace.text = "Pace: $minutes:$seconds"
        }
    }

    //Format milliseconds to hours:minutes:seconds returned as a string
    private fun msToTimeFormat(a: Long): String {
        var result = ""
        var seconds = 0
        var minutes = 0
        var hours = 0

        seconds = ((a / 1000) % 60).toInt()
        minutes = ((a / (1000 * 60)) % 60).toInt()
        hours = ((a / (1000 * 60 * 60)) % 24).toInt()

        var sSeconds = seconds.toString()
        var sMinutes = minutes.toString()
        var sHours = hours.toString()

        when {
            seconds < 10 -> sSeconds = "0$seconds"
            minutes < 10 -> sMinutes = "0$minutes"
            hours < 10 -> sHours = "0$hours"
        }

        result = if (hours == 0)
            "$sMinutes:$sSeconds"
        else
            "$sHours:$sMinutes:$sSeconds"


        return result
    }
}
