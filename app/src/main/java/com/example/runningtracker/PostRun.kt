package com.example.runningtracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class PostRun : AppCompatActivity() {

    private lateinit var distance: TextView
    private lateinit var totalTime: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_run)
        distance = findViewById(R.id.distance)
        totalTime = findViewById(R.id.totalTime)

        val dist = intent.getStringExtra("Distance")
        val time = intent.getStringExtra("Time")
        distance.text = "Distance: " + dist
        totalTime.text = "Total time: " + time
    }
}


